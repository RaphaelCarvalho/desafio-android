package com.carvalho.raphael.desafioandroid_github.ws.pullrequest;

import android.support.test.InstrumentationRegistry;

import com.carvalho.raphael.desafioandroid_github.config.URLsUtil;
import com.carvalho.raphael.desafioandroid_github.model.pullrequest.PullRequest;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;
import java.util.Locale;

import static org.junit.Assert.*;

/**
 * Created by RaphaelCarvalho on 26/07/2017.
 */
public class PullRequestWSTest {
    private static PullRequestWS mWS;

    @BeforeClass
    public static void beforeClass() throws NoSuchFieldException {
        mWS = new PullRequestWS(InstrumentationRegistry.getContext());
    }

    @AfterClass
    public static void afterClass() {
        mWS = null;
    }

    @Test
    public void carregarPullRequests() throws Exception {//FIXME precisa criar mocks
        String url = URLsUtil.getPullRequestPagUrl("https://api.github.com/repos/ReactiveX/RxJava/pulls", 1);
        List<PullRequest> pulls = mWS.carregarPullRequests(url);

        assertFalse(pulls.isEmpty());

        PullRequest pr = pulls.get(0);
        assertEquals("akarnokd", pr.getNomeAutor());
        assertEquals("https://avatars2.githubusercontent.com/u/1269832?v=4", pr.getUrlFotoAutor());

        assertEquals("2.x: add missing null check to fused Observable.fromCallable", pr.getTitulo());
        assertTrue(pr.getDescricao().startsWith("There was a missing null check on the fusion"));
        assertEquals("2017-07-25T13:59:28Z", pr.getData());

        assertEquals("https://github.com/ReactiveX/RxJava/pull/5517", pr.getHtmlUrl());
    }

}