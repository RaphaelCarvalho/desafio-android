package com.carvalho.raphael.desafioandroid_github.ws.repositorio;

import android.support.test.InstrumentationRegistry;

import com.carvalho.raphael.desafioandroid_github.config.URLsUtil;
import com.carvalho.raphael.desafioandroid_github.model.repositorio.Repositorio;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;
import java.util.Locale;

import static org.junit.Assert.*;

/**
 * Created by RaphaelCarvalho on 26/07/2017.
 */
public class RepositorioWSTest {
    private static RepositorioWS mWS;

    @BeforeClass
    public static void beforeClass() throws NoSuchFieldException {
        mWS = new RepositorioWS(InstrumentationRegistry.getContext());
    }

    @AfterClass
    public static void afterClass() {
        mWS = null;
    }

    @Test
    public void carregarRepositorios() throws Exception {//FIXME precisa criar mocks
        String url = URLsUtil.getRepositorioUrl(1);
        List<Repositorio> repositorios = mWS.carregarRepositorios(url);

        assertFalse(repositorios.isEmpty());

        Repositorio r = repositorios.get(0);
        assertEquals("RxJava", r.getNome());
        assertTrue(r.getDescricao().startsWith("RxJava – Reactive Extensions"));

        assertEquals("ReactiveX", r.getNomeAutor());
        assertEquals("https://avatars1.githubusercontent.com/u/6407041?v=4", r.getUrlFotoAutor());

        assertEquals(26040, r.getStars());
        assertEquals(4601, r.getForks());

        assertEquals("https://api.github.com/repos/ReactiveX/RxJava/pulls", r.getPullsUrl());
    }
}