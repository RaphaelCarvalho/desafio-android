package com.carvalho.raphael.desafioandroid_github.ws.pullrequest;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.carvalho.raphael.desafioandroid_github.model.pullrequest.PullRequest;
import com.carvalho.raphael.desafioandroid_github.util.gson.InnerJsonDeserializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by RaphaelCarvalho on 26/07/2017.
 */

public class PullRequestWS {
    private final RequestQueue requestQueue;
    private final Gson gson;

    public PullRequestWS(Context context) {//AppContext
        requestQueue = Volley.newRequestQueue(context);
        gson = new GsonBuilder()
                .registerTypeAdapter(
                        PullRequest.class,
                        new InnerJsonDeserializer<>(PullRequest.class))
                .create();
    }

    public List<PullRequest> carregarPullRequests(String url) throws ExecutionException, InterruptedException {
        RequestFuture<String> future = RequestFuture.newFuture();
        StringRequest request = new StringRequest(Request.Method.GET, url, future, future);
        requestQueue.add(request);

        String pullRequestsJSON = future.get();
        return Arrays.asList(gson.fromJson(pullRequestsJSON, PullRequest[].class));
    }
}
