package com.carvalho.raphael.desafioandroid_github.activity.pullrequest.adapter;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.carvalho.raphael.desafioandroid_github.R;
import com.carvalho.raphael.desafioandroid_github.model.pullrequest.PullRequest;
import com.carvalho.raphael.desafioandroid_github.util.crash.CrashlyticsUtil;
import com.carvalho.raphael.desafioandroid_github.util.viewholder.LoadingViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RaphaelCarvalho on 26/07/2017.
 */
public class PullRequestAdapter extends RecyclerView.Adapter {
    private final List<PullRequest> pulls;
    private IPullRequestAdapter mIPullRequestAdapter;

    public PullRequestAdapter(@NonNull IPullRequestAdapter IPullRequestAdapter) {
        pulls = new ArrayList<>();
        this.mIPullRequestAdapter = IPullRequestAdapter;
    }

    @Override
    public int getItemViewType(int position) {
        return pulls.get(position) == null
                ? LoadingViewHolder.TYPE
                : PullRequestViewHolder.TYPE;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        switch (viewType) {
            case LoadingViewHolder.TYPE:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.load_centralizado, parent, false);
                return new LoadingViewHolder(view);

            case PullRequestViewHolder.TYPE:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.viewholder_pull_request, parent, false);
                return new PullRequestViewHolder(view, mIPullRequestAdapter);

            default:
                CrashlyticsUtil.crash(new IllegalStateException("Tipo invalido: " + viewType));
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PullRequestViewHolder) {
            ((PullRequestViewHolder) holder).update(pulls.get(position));

            if (position + 1 == getItemCount()) { // Ultimo repositorio carregado
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        mIPullRequestAdapter.onLastItemLoaded();
                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return pulls.size();
    }

    public void clear() {
        pulls.clear();
        notifyDataSetChanged();
    }

    public void exbirLoad() {
        pulls.add(null);
        notifyItemInserted(pulls.size() - 1);
    }

    public void esconderLoad() {
        if (pulls.isEmpty()) return;

        int tamAntesAlteracao = pulls.size();
        PullRequest remove = pulls.remove(tamAntesAlteracao - 1);
        if (remove != null)
            CrashlyticsUtil.crash(new IllegalStateException("Item removido invalido"));

        notifyItemRemoved(pulls.size());
    }

    public void update(List<PullRequest> repositorios) {
        if (repositorios == null || repositorios.isEmpty()) return;

        int primeiroIndiceAdd = repositorios.size();
        this.pulls.addAll(repositorios);
        int qtdItensAdd = repositorios.size() - primeiroIndiceAdd;

        notifyItemRangeInserted(primeiroIndiceAdd, qtdItensAdd);
    }

    public List<PullRequest> getValues() {
        return pulls;
    }

    public interface IPullRequestAdapter {
        void onSelectedItem(PullRequest pull);

        void onLastItemLoaded();
    }
}
