package com.carvalho.raphael.desafioandroid_github.model.repositorio;

import android.text.TextUtils;

import com.carvalho.raphael.desafioandroid_github.util.gson.FieldTreatedGsonDeserialization;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rapha on 25/07/2017.
 */

public class Repositorio {
    @SerializedName("name")
    private String nome;
    @SerializedName("description")
    private String descricao;

    @SerializedName("login")
    @FieldTreatedGsonDeserialization(objectName = "owner")
    private String nomeAutor;
    @SerializedName("avatar_url")
    @FieldTreatedGsonDeserialization(objectName = "owner")
    private String urlFotoAutor;

    @SerializedName("stargazers_count")
    private long stars;
    @SerializedName("forks_count")
    private long forks;

    @SerializedName("pulls_url")
    private String pullsUrl;

    public String getNome() {
        return nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public String getNomeAutor() {
        return nomeAutor;
    }

    public String getUrlFotoAutor() {
        return urlFotoAutor;
    }

    public long getStars() {
        return stars;
    }

    public long getForks() {
        return forks;
    }

    public String getPullsUrl() {
        if (TextUtils.isEmpty(pullsUrl)) return null;
        return pullsUrl.replace("{/number}", "");
    }
}
