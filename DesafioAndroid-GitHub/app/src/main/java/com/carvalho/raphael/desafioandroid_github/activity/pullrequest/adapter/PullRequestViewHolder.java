package com.carvalho.raphael.desafioandroid_github.activity.pullrequest.adapter;

import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.carvalho.raphael.desafioandroid_github.R;
import com.carvalho.raphael.desafioandroid_github.activity.pullrequest.adapter.PullRequestAdapter.IPullRequestAdapter;
import com.carvalho.raphael.desafioandroid_github.model.pullrequest.PullRequest;
import com.carvalho.raphael.desafioandroid_github.util.crash.CrashlyticsUtil;

import java.text.ParseException;

/**
 * Created by RaphaelCarvalho on 27/07/2017.
 */
class PullRequestViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    static final int TYPE = 1;

    private IPullRequestAdapter mIAdapter;
    private PullRequest pullRequest;

    private TextView tvPullTitulo;
    private TextView tvPullDescricao;

    private TextView tvDataPull;

    private ImageView ivFotoAutor;
    private TextView tvNomeAutor;

    PullRequestViewHolder(View itemView, IPullRequestAdapter iAdapter) {
        super(itemView);
        itemView.setOnClickListener(this);

        this.mIAdapter = iAdapter;

        tvPullTitulo = (TextView) itemView.findViewById(R.id.tvPullTitulo);
        tvPullDescricao = (TextView) itemView.findViewById(R.id.tvPullDescricao);

        ivFotoAutor = (ImageView) itemView.findViewById(R.id.ivFotoAutor);
        tvNomeAutor = (TextView) itemView.findViewById(R.id.tvNomeAutor);

        tvDataPull = (TextView) itemView.findViewById(R.id.tvDataPull);
    }

    void update(PullRequest pullRequest) {
        this.pullRequest = pullRequest;

        tvPullTitulo.setText(pullRequest.getTitulo());
        tvPullDescricao.setText(pullRequest.getDescricao());

        try {
            tvDataPull.setText(pullRequest.getData());

        } catch (Exception e) {
            CrashlyticsUtil.crash(e);
            tvDataPull.setText("erro");
        }

        Glide.with(ivFotoAutor.getContext())
                .load(pullRequest.getUrlFotoAutor())
                .asBitmap()
                .centerCrop()
                .placeholder(R.mipmap.img_user_placeholder)
                .into(new BitmapImageViewTarget(ivFotoAutor) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(ivFotoAutor.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        ivFotoAutor.setImageDrawable(circularBitmapDrawable);
                    }
                });
        tvNomeAutor.setText(pullRequest.getNomeAutor());
    }

    @Override
    public void onClick(View v) {
        mIAdapter.onSelectedItem(pullRequest);
    }
}
