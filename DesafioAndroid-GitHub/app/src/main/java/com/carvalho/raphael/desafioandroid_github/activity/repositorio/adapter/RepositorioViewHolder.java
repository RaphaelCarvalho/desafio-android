package com.carvalho.raphael.desafioandroid_github.activity.repositorio.adapter;

import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.carvalho.raphael.desafioandroid_github.R;
import com.carvalho.raphael.desafioandroid_github.activity.repositorio.adapter.RepositorioAdapter.IRepositorioAdapter;
import com.carvalho.raphael.desafioandroid_github.model.repositorio.Repositorio;
import com.carvalho.raphael.desafioandroid_github.util.format.Format;

/**
 * Created by RaphaelCarvalho on 27/07/2017.
 */
class RepositorioViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    static final int TYPE = 1;

    private IRepositorioAdapter mIRepositorioAdapter;
    private Repositorio repositorio;

    private TextView tvRepositorioNome;
    private TextView tvRepositorioDescricao;

    private TextView tvStars;
    private TextView tvForks;

    private ImageView ivFotoAutor;
    private TextView tvNomeAutor;

    RepositorioViewHolder(View itemView, IRepositorioAdapter IRepositorioAdapter) {
        super(itemView);
        itemView.setOnClickListener(this);

        this.mIRepositorioAdapter = IRepositorioAdapter;

        tvRepositorioNome = (TextView) itemView.findViewById(R.id.tvRepositorioNome);
        tvRepositorioDescricao = (TextView) itemView.findViewById(R.id.tvRepositorioDescricao);

        tvForks = (TextView) itemView.findViewById(R.id.tvForks);
        tvStars = (TextView) itemView.findViewById(R.id.tvStars);

        ivFotoAutor = (ImageView) itemView.findViewById(R.id.ivFotoAutor);
        tvNomeAutor = (TextView) itemView.findViewById(R.id.tvNomeAutor);
    }

    void update(Repositorio repositorio) {
        this.repositorio = repositorio;

        tvRepositorioNome.setText(repositorio.getNome());
        tvRepositorioDescricao.setText(repositorio.getDescricao());

        tvStars.setText(Format.formatarNum(tvStars.getContext(), repositorio.getStars()));
        tvForks.setText(Format.formatarNum(tvForks.getContext(), repositorio.getForks()));

        Glide.with(ivFotoAutor.getContext())
                .load(repositorio.getUrlFotoAutor())
                .asBitmap()
                .centerCrop()
                .placeholder(R.mipmap.img_user_placeholder)
                .into(new BitmapImageViewTarget(ivFotoAutor) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(ivFotoAutor.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        ivFotoAutor.setImageDrawable(circularBitmapDrawable);
                    }
                });
        tvNomeAutor.setText(repositorio.getNomeAutor());
    }

    @Override
    public void onClick(View v) {
        mIRepositorioAdapter.onSelectedItem(repositorio);
    }
}
