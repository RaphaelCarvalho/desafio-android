package com.carvalho.raphael.desafioandroid_github.activity.pullrequest.delegate;

import android.content.Context;

import com.carvalho.raphael.desafioandroid_github.controller.pullrequest.PullRequestController;
import com.carvalho.raphael.desafioandroid_github.model.pullrequest.PullRequest;
import com.carvalho.raphael.desafioandroid_github.util.task.TaskUtils;

import java.util.List;

/**
 * Created by RaphaelCarvalho on 26/07/2017.
 */

public class PullRequestDelegate {
    private final PullRequestController controller;
    private final IPullRequestDelegate mIDelegate;

    private String urlBase;
    private int pagination;

    private PullRequestTask task;

    public PullRequestDelegate(Context context, IPullRequestDelegate iDelegate,
                               String urlBase, int pagination) {
        controller = new PullRequestController(context);
        this.mIDelegate = iDelegate;

        this.urlBase = urlBase;
        this.pagination = pagination;
    }

    public synchronized void reiniciar() {
        pagination = 0;
        carregarMais(true);
    }

    public synchronized void carregarMais() {
        carregarMais(false);
    }

    private synchronized void carregarMais(boolean force) {
        if (TaskUtils.isFinished(task)
                || force) {
            task = new PullRequestTask(controller, mIDelegate, urlBase, ++pagination);
            task.execute();
        }
    }

    public void release() {
        TaskUtils.cancel(task);
    }

    public interface IPullRequestDelegate {
        void iniciadoCarregamento();

        void finalizado(List<PullRequest> pulls, int pagination);

        void finalizado(Exception e);
    }
}
