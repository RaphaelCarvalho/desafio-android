package com.carvalho.raphael.desafioandroid_github.activity.pullrequest.delegate;

import android.os.AsyncTask;

import com.carvalho.raphael.desafioandroid_github.activity.pullrequest.delegate.PullRequestDelegate.IPullRequestDelegate;
import com.carvalho.raphael.desafioandroid_github.config.URLsUtil;
import com.carvalho.raphael.desafioandroid_github.controller.pullrequest.PullRequestController;
import com.carvalho.raphael.desafioandroid_github.model.pullrequest.PullRequest;

import java.util.List;

/**
 * Created by RaphaelCarvalho on 27/07/2017.
 */

class PullRequestTask extends AsyncTask<Void, Void, List<PullRequest>> {
    private PullRequestController controller;
    private IPullRequestDelegate mIDelegate;

    private final String urlBase;
    private final int pagination;
    private Exception exc;

    PullRequestTask(PullRequestController controller, IPullRequestDelegate iDelegate, String urlBase, int pagination) {
        if (pagination <= 0)
            throw new IllegalArgumentException("Pagination invalida: " + pagination);

        this.controller = controller;
        mIDelegate = iDelegate;

        this.urlBase = urlBase;
        this.pagination = pagination;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mIDelegate.iniciadoCarregamento();
    }

    @Override
    protected List<PullRequest> doInBackground(Void... params) {
        try {
            String url = URLsUtil.getPullRequestPagUrl(urlBase, pagination);
            return controller.carregarPullRequests(url);

        } catch (Exception e) {
            exc = e;
        }

        return null;
    }

    @Override
    protected void onPostExecute(List<PullRequest> pulls) {
        super.onPostExecute(pulls);


        if (exc == null) {
            mIDelegate.finalizado(pulls, pagination);

        } else mIDelegate.finalizado(exc);
    }
}
