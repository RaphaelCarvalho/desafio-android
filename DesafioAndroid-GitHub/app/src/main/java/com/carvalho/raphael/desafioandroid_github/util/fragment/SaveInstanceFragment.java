package com.carvalho.raphael.desafioandroid_github.util.fragment;

import android.app.Fragment;

/**
 * Created by RaphaelCarvalho on 27/07/2017.
 */

public class SaveInstanceFragment<T> extends Fragment {
    private T value;

    public SaveInstanceFragment() {
        setRetainInstance(true);
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}
