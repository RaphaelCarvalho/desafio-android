package com.carvalho.raphael.desafioandroid_github.util.gson;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by RaphaelCarvalho on 26/07/2017.
 * Usado para informar quais campos precisam ser tratados na desserialização
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface FieldTreatedGsonDeserialization {
    String objectName();
}
