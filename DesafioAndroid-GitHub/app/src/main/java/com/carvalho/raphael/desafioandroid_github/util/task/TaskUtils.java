package com.carvalho.raphael.desafioandroid_github.util.task;

import android.os.AsyncTask;

/**
 * Created by rapha on 30/05/2017.
 */

public class TaskUtils {
    public static boolean isFinished(AsyncTask task) {
        return task == null || task.isCancelled() || task.getStatus() == AsyncTask.Status.FINISHED;
    }

    public static void cancel(AsyncTask task) {
        if (task != null) task.cancel(true);
    }
}
