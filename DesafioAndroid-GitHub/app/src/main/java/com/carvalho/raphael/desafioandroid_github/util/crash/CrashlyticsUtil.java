package com.carvalho.raphael.desafioandroid_github.util.crash;

import android.util.Log;

/**
 * Created by RaphaelCarvalho on 26/07/2017.
 * Usado para centralizar os erros para facilitar a integração com o crashlytics por exemplo
 */

public class CrashlyticsUtil {

    public static void crash(Exception e) {
        e.printStackTrace();
    }

    public static void log(String resposta) {
        Log.d("Testando", resposta);
    }
}
