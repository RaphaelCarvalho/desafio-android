package com.carvalho.raphael.desafioandroid_github.util.format;

import android.content.Context;
import android.support.annotation.NonNull;

import com.carvalho.raphael.desafioandroid_github.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by RaphaelCarvalho on 26/07/2017.
 */

public class Format {
    private static final String ISO_8601_24H_FULL_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";

    public static String formatarNum(@NonNull Context context, long num) {
        long k = 1000;
        long m = k * k;
        long b = m * k;
        long t = b * k;

        if (num < k) {
            return num + "";

        } else if (num < m) {
            long temp = num / k;
            return context.getString(R.string.num_format_mil, temp);

        } else if (num < b) {
            long temp = num / m;
            return context.getString(R.string.num_format_milhao, temp);

        } else if (num < t) {
            long temp = num / b;
            return context.getString(R.string.num_format_bilhao, temp);

        } else {
            long temp = num / t;
            return context.getString(R.string.num_format_trilhao, temp);
        }
    }

    public static String formatarData(String data) throws ParseException {
        Date d = new SimpleDateFormat(ISO_8601_24H_FULL_FORMAT, Locale.getDefault()).parse(data);
        return new SimpleDateFormat("MMM d, yyyy", Locale.getDefault()).format(d);
    }
}
