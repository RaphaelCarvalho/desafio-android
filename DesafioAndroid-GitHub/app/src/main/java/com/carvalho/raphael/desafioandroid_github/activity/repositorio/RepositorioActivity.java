package com.carvalho.raphael.desafioandroid_github.activity.repositorio;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.carvalho.raphael.desafioandroid_github.R;
import com.carvalho.raphael.desafioandroid_github.activity.pullrequest.PullRequestActivity;
import com.carvalho.raphael.desafioandroid_github.activity.repositorio.adapter.RepositorioAdapter;
import com.carvalho.raphael.desafioandroid_github.activity.repositorio.delegate.RepositorioDelegate;
import com.carvalho.raphael.desafioandroid_github.model.repositorio.Repositorio;
import com.carvalho.raphael.desafioandroid_github.util.crash.CrashlyticsUtil;
import com.carvalho.raphael.desafioandroid_github.util.fragment.SaveInstanceFragment;

import java.util.List;

public class RepositorioActivity extends AppCompatActivity implements RepositorioAdapter.IRepositorioAdapter, SwipeRefreshLayout.OnRefreshListener, RepositorioDelegate.IRepositorioDelegate {
    private static final String ARG_PAG = "com.carvalho.raphael.desafioandroid_github.arg_pagination";
    private static final String ARG_POSITION = "com.carvalho.raphael.desafioandroid_github.arg_position";
    private static final String ARG_FRAG_LIST = "com.carvalho.raphael.desafioandroid_github.arg_fragment";

    private SwipeRefreshLayout mSrlRepositorio;
    private RecyclerView mRecyclerView;

    private RepositorioDelegate mDelegate;
    private RepositorioAdapter mAdapter;

    private int pag;
    private int recyclePosition;
    private SaveInstanceFragment<List<Repositorio>> mSaveInsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repositorio);

        if (savedInstanceState != null) {
            recarregarEstado(savedInstanceState);

        } else {
            initSaveInstanceFragment();
        }

        initView(mSaveInsFragment.getValue());

        mDelegate = new RepositorioDelegate(getApplicationContext(), this, pag);
        if (savedInstanceState == null) mDelegate.carregarMais();
    }

    private void recarregarEstado(Bundle savedInstanceState) {
        pag = savedInstanceState.getInt(ARG_PAG, 0);
        recyclePosition = savedInstanceState.getInt(ARG_POSITION, 0);

        Fragment fragment = getFragmentManager()
                .getFragment(savedInstanceState, ARG_FRAG_LIST);
        mSaveInsFragment = (SaveInstanceFragment<List<Repositorio>>) fragment;
    }

    private void initSaveInstanceFragment() {
        mSaveInsFragment = new SaveInstanceFragment<>();
        getFragmentManager().beginTransaction().add(R.id.flRetainInstance, mSaveInsFragment, ARG_FRAG_LIST).commit();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(ARG_PAG, pag);
        outState.putInt(ARG_POSITION, mRecyclerView.getVerticalScrollbarPosition());
        super.onSaveInstanceState(outState);

        mSaveInsFragment.setValue(mAdapter.getValues());
        getFragmentManager().putFragment(outState, ARG_FRAG_LIST, mSaveInsFragment);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mDelegate.release();
    }

    private void initView(List<Repositorio> repositorios) {
        mSrlRepositorio = (SwipeRefreshLayout) findViewById(R.id.srlLayoutList);
        mSrlRepositorio.setOnRefreshListener(this);

        mAdapter = new RepositorioAdapter(this);
        if (repositorios != null
                && !repositorios.isEmpty()) mAdapter.update(repositorios);

        mRecyclerView = (RecyclerView) findViewById(R.id.rvLayoutList);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(
                new LinearLayoutManager(
                        this,
                        LinearLayoutManager.VERTICAL,
                        false));
        mRecyclerView.setVerticalScrollbarPosition(recyclePosition);
    }

    @Override
    public void onSelectedItem(Repositorio repositorio) {
        Intent intent = new Intent(this, PullRequestActivity.class);
        intent.putExtra(PullRequestActivity.ARG_PULL_URL, repositorio.getPullsUrl());
        intent.putExtra(PullRequestActivity.ARG_PULL_TITLE, repositorio.getNome());

        startActivity(intent);
    }

    @Override
    public void onLastItemLoaded() {
        mDelegate.carregarMais();
    }

    @Override
    public void onRefresh() {
        mAdapter.clear();
        mDelegate.reiniciar();
    }

    @Override
    public void iniciadoCarregamento() {
        mAdapter.exbirLoad();
    }

    @Override
    public void finalizado(List<Repositorio> repositorios, int pagination) {
        if (mSrlRepositorio.isRefreshing()) mSrlRepositorio.setRefreshing(false);
        mAdapter.esconderLoad();

        mAdapter.update(repositorios);
        pag = pagination;
    }

    @Override
    public void finalizado(Exception e) {
        if (mSrlRepositorio.isRefreshing()) mSrlRepositorio.setRefreshing(false);
        mAdapter.esconderLoad();

        CrashlyticsUtil.crash(e);
        Toast.makeText(this, R.string.erro_carregamento_repositorios, Toast.LENGTH_SHORT).show();
    }
}
