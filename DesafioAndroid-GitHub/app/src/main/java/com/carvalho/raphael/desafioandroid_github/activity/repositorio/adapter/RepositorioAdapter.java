package com.carvalho.raphael.desafioandroid_github.activity.repositorio.adapter;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.carvalho.raphael.desafioandroid_github.R;
import com.carvalho.raphael.desafioandroid_github.model.repositorio.Repositorio;
import com.carvalho.raphael.desafioandroid_github.util.crash.CrashlyticsUtil;
import com.carvalho.raphael.desafioandroid_github.util.viewholder.LoadingViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RaphaelCarvalho on 26/07/2017.
 */
public class RepositorioAdapter extends RecyclerView.Adapter {
    private final List<Repositorio> repositorios;
    private IRepositorioAdapter mIRepositorioAdapter;

    public RepositorioAdapter(@NonNull IRepositorioAdapter IRepositorioAdapter) {
        repositorios = new ArrayList<>();
        this.mIRepositorioAdapter = IRepositorioAdapter;
    }

    @Override
    public int getItemViewType(int position) {
        return repositorios.get(position) == null
                ? LoadingViewHolder.TYPE
                : RepositorioViewHolder.TYPE;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        switch (viewType) {
            case LoadingViewHolder.TYPE:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.load_centralizado, parent, false);
                return new LoadingViewHolder(view);

            case RepositorioViewHolder.TYPE:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.viewholder_repositorio, parent, false);
                return new RepositorioViewHolder(view, mIRepositorioAdapter);

            default:
                CrashlyticsUtil.crash(new IllegalStateException("Tipo invalido: " + viewType));
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof RepositorioViewHolder) {
            ((RepositorioViewHolder) holder).update(repositorios.get(position));

            if (position + 1 == getItemCount()) { // Ultimo repositorio carregado
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        mIRepositorioAdapter.onLastItemLoaded();
                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return repositorios.size();
    }

    public void clear() {
        repositorios.clear();
        notifyDataSetChanged();
    }

    public void exbirLoad() {
        repositorios.add(null);
        notifyItemInserted(repositorios.size() - 1);
    }

    public void esconderLoad() {
        if (repositorios.isEmpty()) return;

        int tamAntesAlteracao = repositorios.size();
        Repositorio remove = repositorios.remove(tamAntesAlteracao - 1);
        if (remove != null)
            CrashlyticsUtil.crash(new IllegalStateException("Item removido invalido"));

        notifyItemRemoved(repositorios.size());
    }

    public void update(List<Repositorio> repositorios) {
        if (repositorios == null || repositorios.isEmpty()) return;

        int primeiroIndiceAdd = repositorios.size();
        this.repositorios.addAll(repositorios);
        int qtdItensAdd = repositorios.size() - primeiroIndiceAdd;

        notifyItemRangeInserted(primeiroIndiceAdd, qtdItensAdd);
    }

    public List<Repositorio> getValues() {
        return  repositorios;
    }

    public interface IRepositorioAdapter {
        void onSelectedItem(Repositorio repositorio);

        void onLastItemLoaded();
    }
}
