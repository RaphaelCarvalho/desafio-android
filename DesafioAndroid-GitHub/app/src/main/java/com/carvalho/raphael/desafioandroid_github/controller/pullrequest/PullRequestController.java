package com.carvalho.raphael.desafioandroid_github.controller.pullrequest;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.carvalho.raphael.desafioandroid_github.model.pullrequest.PullRequest;
import com.carvalho.raphael.desafioandroid_github.util.gson.InnerJsonDeserializer;
import com.carvalho.raphael.desafioandroid_github.ws.pullrequest.PullRequestWS;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by RaphaelCarvalho on 26/07/2017.
 */

public class PullRequestController {
    private final PullRequestWS pullRequestWS;

    public PullRequestController(Context context) {
        pullRequestWS = new PullRequestWS(context);
    }

    public List<PullRequest> carregarPullRequests(String url) throws ExecutionException, InterruptedException {
        return pullRequestWS.carregarPullRequests(url);
    }
}
