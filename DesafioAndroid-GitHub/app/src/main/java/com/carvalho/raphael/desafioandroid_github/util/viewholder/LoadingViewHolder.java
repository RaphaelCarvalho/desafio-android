package com.carvalho.raphael.desafioandroid_github.util.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by RaphaelCarvalho on 26/07/2017.
 */

public class LoadingViewHolder extends RecyclerView.ViewHolder {
    public static final int TYPE = 1000;

    public LoadingViewHolder(View itemView) {
        super(itemView);
    }
}
