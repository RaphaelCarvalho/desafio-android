package com.carvalho.raphael.desafioandroid_github.activity.repositorio.delegate;

import android.os.AsyncTask;

import com.carvalho.raphael.desafioandroid_github.config.URLsUtil;
import com.carvalho.raphael.desafioandroid_github.controller.repositorio.RepositorioController;
import com.carvalho.raphael.desafioandroid_github.model.repositorio.Repositorio;

import java.util.List;

/**
 * Created by RaphaelCarvalho on 27/07/2017.
 */

public class RepositorioTask extends AsyncTask<Void, Void, List<Repositorio>> {
    private RepositorioController controller;
    private RepositorioDelegate.IRepositorioDelegate mIDelegate;

    private final int pagination;
    private Exception exc;

    public RepositorioTask(RepositorioController controller, RepositorioDelegate.IRepositorioDelegate iDelegate, int pagination) {
        if (pagination <= 0)
            throw new IllegalArgumentException("Pagination invalida: " + pagination);

        this.controller = controller;
        mIDelegate = iDelegate;
        this.pagination = pagination;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mIDelegate.iniciadoCarregamento();
    }

    @Override
    protected List<Repositorio> doInBackground(Void... params) {
        try {
            String url = URLsUtil.getRepositorioUrl(pagination);
            return controller.carregarRepositorios(url);

        } catch (Exception e) {
            exc = e;
        }

        return null;
    }

    @Override
    protected void onPostExecute(List<Repositorio> repositorios) {
        super.onPostExecute(repositorios);


        if (exc == null) {
            mIDelegate.finalizado(repositorios, pagination);

        } else mIDelegate.finalizado(exc);
    }
}
