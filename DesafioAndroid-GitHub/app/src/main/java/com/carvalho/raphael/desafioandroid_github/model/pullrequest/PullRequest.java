package com.carvalho.raphael.desafioandroid_github.model.pullrequest;

import com.carvalho.raphael.desafioandroid_github.util.format.Format;
import com.carvalho.raphael.desafioandroid_github.util.gson.FieldTreatedGsonDeserialization;
import com.google.gson.annotations.SerializedName;

import java.text.ParseException;

/**
 * Created by rapha on 25/07/2017.
 */

public class PullRequest {
    @SerializedName("login")
    @FieldTreatedGsonDeserialization(objectName = "user")
    private String nomeAutor;
    @SerializedName("avatar_url")
    @FieldTreatedGsonDeserialization(objectName = "user")
    private String urlFotoAutor;

    @SerializedName("title")
    private String titulo;
    @SerializedName("body")
    private String descricao;
    @SerializedName("created_at")
    private String data;

    @SerializedName("html_url")
    private String htmlUrl;

    public String getNomeAutor() {
        return nomeAutor;
    }

    public String getUrlFotoAutor() {
        return urlFotoAutor;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public String getData() throws ParseException {
        return Format.formatarData(data);
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }
}
