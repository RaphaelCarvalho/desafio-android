package com.carvalho.raphael.desafioandroid_github.config;

import java.util.Locale;

/**
 * Created by RaphaelCarvalho on 26/07/2017.
 */

public class URLsUtil {
    private static final String REPOSITORIO_URL = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=%1$d";
    public static final String PAG_PULL_REQUEST_URL = "?state=all&page=%1$d";

    public static String getRepositorioUrl(int pagination) {
        return String.format(Locale.ENGLISH, REPOSITORIO_URL, pagination);
    }

    public static String getPullRequestPagUrl(String urlBase, int pagination) {
        return  String.format(Locale.ENGLISH, urlBase + URLsUtil.PAG_PULL_REQUEST_URL, pagination);
    }
}
