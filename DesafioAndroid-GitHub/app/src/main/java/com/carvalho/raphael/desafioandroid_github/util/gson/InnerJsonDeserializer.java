package com.carvalho.raphael.desafioandroid_github.util.gson;

import com.carvalho.raphael.desafioandroid_github.util.crash.CrashlyticsUtil;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by RaphaelCarvalho on 26/07/2017.
 */

public class InnerJsonDeserializer <T> implements JsonDeserializer<T>  {
    private final Class<T> _class;
    private final Gson gson;

    private final Map<String, List<Field>> fields;

    public InnerJsonDeserializer(Class<T> _class) {
        this._class = _class;
        this.gson = new Gson();

        fields = new HashMap<>();
        separarFields();
    }

    private void separarFields() {
        FieldTreatedGsonDeserialization annotation;
        for (Field field : _class.getDeclaredFields()) {
            if (field.isAnnotationPresent(FieldTreatedGsonDeserialization.class)) {
                annotation = field.getAnnotation(FieldTreatedGsonDeserialization.class);
                String objectName = annotation.objectName();

                if (!fields.containsKey(objectName)) fields.put(objectName, new ArrayList<Field>());

                field.setAccessible(true);
                fields.get(objectName).add(field);
            }
        }
    }

    @Override
    public T deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
            throws JsonParseException {
        T obj = gson.fromJson(json, _class);

        try {
            deserializeFields(obj, json.getAsJsonObject());

        } catch (IllegalAccessException e) {
            CrashlyticsUtil.crash(e);
        }

        return obj;
    }

    private void deserializeFields(T obj, JsonObject jsonObjectCompleto) throws IllegalAccessException {
        SerializedName annotation;
        JsonObject jsonObject;

        for (String key : fields.keySet()) {
            jsonObject = jsonObjectCompleto.getAsJsonObject(key);

            for (Field f : fields.get(key)) {
                annotation = f.getAnnotation(SerializedName.class);
                String snName = annotation.value();

                f.set(obj, jsonObject.get(snName).getAsString());
            }
        }
    }
}
