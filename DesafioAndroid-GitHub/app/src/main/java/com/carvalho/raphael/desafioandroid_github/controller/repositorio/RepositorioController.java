package com.carvalho.raphael.desafioandroid_github.controller.repositorio;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.carvalho.raphael.desafioandroid_github.model.pullrequest.PullRequest;
import com.carvalho.raphael.desafioandroid_github.model.repositorio.Repositorio;
import com.carvalho.raphael.desafioandroid_github.util.gson.InnerJsonDeserializer;
import com.carvalho.raphael.desafioandroid_github.ws.pullrequest.PullRequestWS;
import com.carvalho.raphael.desafioandroid_github.ws.repositorio.RepositorioWS;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by RaphaelCarvalho on 26/07/2017.
 */

public class RepositorioController {
    private final RepositorioWS repositorioWS;

    public RepositorioController(Context context) {
        repositorioWS = new RepositorioWS(context);
    }

    public List<Repositorio> carregarRepositorios(String url) throws ExecutionException, InterruptedException, JSONException {
        return repositorioWS.carregarRepositorios(url);
    }
}
