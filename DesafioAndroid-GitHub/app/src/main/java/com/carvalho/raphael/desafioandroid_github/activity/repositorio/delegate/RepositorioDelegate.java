package com.carvalho.raphael.desafioandroid_github.activity.repositorio.delegate;

import android.content.Context;

import com.carvalho.raphael.desafioandroid_github.controller.repositorio.RepositorioController;
import com.carvalho.raphael.desafioandroid_github.model.repositorio.Repositorio;
import com.carvalho.raphael.desafioandroid_github.util.task.TaskUtils;

import java.util.List;

/**
 * Created by RaphaelCarvalho on 26/07/2017.
 */

public class RepositorioDelegate {
    private final RepositorioController controller;
    private final IRepositorioDelegate mIDelegate;

    private int pagination;
    private RepositorioTask task;

    public RepositorioDelegate(Context context, IRepositorioDelegate iDelegate, int pagination) {
        controller = new RepositorioController(context);
        this.mIDelegate = iDelegate;
        this.pagination = pagination;
    }

    public synchronized void reiniciar() {
        pagination = 0;
        carregarMais(true);
    }

    public synchronized void carregarMais() {
        carregarMais(false);
    }

    private synchronized void carregarMais(boolean force) {
        if (TaskUtils.isFinished(task)
                || force) {
            task = new RepositorioTask(controller, mIDelegate, ++pagination);
            task.execute();
        }
    }

    public void release() {
        TaskUtils.cancel(task);
    }

    public interface IRepositorioDelegate {
        void iniciadoCarregamento();

        void finalizado(List<Repositorio> repositorios, int pagination);

        void finalizado(Exception e);
    }
}
