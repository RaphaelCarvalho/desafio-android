package com.carvalho.raphael.desafioandroid_github.activity.pullrequest;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.carvalho.raphael.desafioandroid_github.R;
import com.carvalho.raphael.desafioandroid_github.activity.pullrequest.adapter.PullRequestAdapter;
import com.carvalho.raphael.desafioandroid_github.activity.pullrequest.delegate.PullRequestDelegate;
import com.carvalho.raphael.desafioandroid_github.model.pullrequest.PullRequest;
import com.carvalho.raphael.desafioandroid_github.util.crash.CrashlyticsUtil;
import com.carvalho.raphael.desafioandroid_github.util.fragment.SaveInstanceFragment;

import java.util.List;

public class PullRequestActivity extends AppCompatActivity implements PullRequestDelegate.IPullRequestDelegate, SwipeRefreshLayout.OnRefreshListener, PullRequestAdapter.IPullRequestAdapter {
    public static final String ARG_PULL_URL = "com.carvalho.raphael.desafioandroid_github.pullsUrl";
    public static final String ARG_PULL_TITLE = "com.carvalho.raphael.desafioandroid_github.pullTitle";

    private static final String SI_PAG = "com.carvalho.raphael.desafioandroid_github.arg_pagination";
    private static final String SI_POSITION = "com.carvalho.raphael.desafioandroid_github.arg_position";
    private static final String SI_FRAG_LIST = "com.carvalho.raphael.desafioandroid_github.arg_fragment";

    private SwipeRefreshLayout mSrlPull;
    private RecyclerView mRecyclerView;

    private PullRequestDelegate mDelegate;
    private PullRequestAdapter mAdapter;

    private int pag;
    private int recyclePosition;
    private SaveInstanceFragment<List<PullRequest>> mSaveInsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_request);

        String title = getIntent().getStringExtra(ARG_PULL_TITLE);
        initToolbar(title);

        if (savedInstanceState != null) {
            recarregarEstado(savedInstanceState);

        } else {
            initSaveInstanceFragment();
        }

        initView(mSaveInsFragment.getValue());

        String urlBase = getIntent().getStringExtra(ARG_PULL_URL);
        mDelegate = new PullRequestDelegate(getApplicationContext(), this, urlBase, pag);
        if (savedInstanceState == null) mDelegate.carregarMais();
    }

    private void initToolbar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(title);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_action_back);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        finish();
        return super.onOptionsItemSelected(item);
    }

    private void recarregarEstado(Bundle savedInstanceState) {
        pag = savedInstanceState.getInt(SI_PAG, 0);
        recyclePosition = savedInstanceState.getInt(SI_POSITION, 0);

        Fragment fragment = getFragmentManager()
                .getFragment(savedInstanceState, SI_FRAG_LIST);
        mSaveInsFragment = (SaveInstanceFragment<List<PullRequest>>) fragment;
    }

    private void initSaveInstanceFragment() {
        mSaveInsFragment = new SaveInstanceFragment<>();
        getFragmentManager().beginTransaction().add(R.id.flRetainInstance, mSaveInsFragment, SI_FRAG_LIST).commit();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(SI_PAG, pag);
        outState.putInt(SI_POSITION, mRecyclerView.getVerticalScrollbarPosition());
        super.onSaveInstanceState(outState);

        mSaveInsFragment.setValue(mAdapter.getValues());
        getFragmentManager().putFragment(outState, SI_FRAG_LIST, mSaveInsFragment);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mDelegate.release();
    }

    private void initView(List<PullRequest> pulls) {
        mSrlPull = (SwipeRefreshLayout) findViewById(R.id.srlLayoutList);
        mSrlPull.setOnRefreshListener(this);

        mAdapter = new PullRequestAdapter(this);
        if (pulls != null
                && !pulls.isEmpty()) mAdapter.update(pulls);

        mRecyclerView = (RecyclerView) findViewById(R.id.rvLayoutList);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(
                new LinearLayoutManager(
                        this,
                        LinearLayoutManager.VERTICAL,
                        false));
        mRecyclerView.setVerticalScrollbarPosition(recyclePosition);
    }

    @Override
    public void onSelectedItem(PullRequest pull) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW);
        browserIntent.setData(Uri.parse(pull.getHtmlUrl()));
        startActivity(browserIntent);
    }

    @Override
    public void onLastItemLoaded() {
        mDelegate.carregarMais();
    }

    @Override
    public void onRefresh() {
        mAdapter.clear();
        mDelegate.reiniciar();
    }

    @Override
    public void iniciadoCarregamento() {
        mAdapter.exbirLoad();
    }

    @Override
    public void finalizado(List<PullRequest> pulls, int pagination) {
        if (mSrlPull.isRefreshing()) mSrlPull.setRefreshing(false);
        mAdapter.esconderLoad();

        mAdapter.update(pulls);
        pag = pagination;
    }

    @Override
    public void finalizado(Exception e) {
        if (mSrlPull.isRefreshing()) mSrlPull.setRefreshing(false);
        mAdapter.esconderLoad();

        CrashlyticsUtil.crash(e);
        Toast.makeText(this, R.string.erro_carregamento_pull, Toast.LENGTH_SHORT).show();
    }
}
